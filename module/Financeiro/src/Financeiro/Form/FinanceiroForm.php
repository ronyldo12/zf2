<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Financeiro
 *
 * @author Ronyldo12
 */
namespace Financeiro\Form;

use Zend\Form\Form;

class FinanceiroForm extends Form
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct('financeiro');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        $this->add(array(
            'name' => 'conta',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Conta Corrente',
            ),
        ));
        $this->add(array(
            'name' => 'valor',
            'attributes' => array(
                'type'  => 'double',
            ),
            'options' => array(
                'label' => 'Valor',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Salvar',
                'id' => 'submitbutton',
            ),
        ));
    }
}