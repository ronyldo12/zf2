<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Financeiro\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController {

    protected $albumTable;

    public function indexAction() {
        return new ViewModel();
    }

    public function addAction() {
        $form = new \Financeiro\Form\FinanceiroForm();
        $form->get('submit')->setValue('Add');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $album = new \Financeiro\Model\Financeiro();
            $form->setInputFilter($album->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $album->exchangeArray($form->getData());
                $this->getFinanceiroTable()->saveAlbum($album);

                // Redirect to list of albums
                return $this->redirect()->toRoute('financeiro');
            }
        }
        return array('form' => $form);
    }

    public function getFinanceiroTable() {
        if (!$this->albumTable) {
            $sm = $this->getServiceLocator();
            $this->albumTable = $sm->get('Financeiro\Model\FinanceiroTable');
        }
        return $this->albumTable;
    }

}
